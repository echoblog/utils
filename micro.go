package utils

import (
	"time"

	"github.com/micro/go-micro/registry"

	"github.com/opentracing/opentracing-go"

	hystrixplugin "github.com/micro/go-plugins/wrapper/breaker/hystrix"
	rateplugin "github.com/micro/go-plugins/wrapper/ratelimiter/uber"
	ocplugin "github.com/micro/go-plugins/wrapper/trace/opentracing"

	"github.com/afex/hystrix-go/hystrix"
	"github.com/micro/go-micro"
	"github.com/micro/go-micro/client"
	"github.com/micro/go-plugins/wrapper/select/roundrobin"
)

// NewMicroClientForWeb new micro rpc client for micro-web
func NewMicroClientForWeb(sw, dt, ept, vt, mc, rate int, registry *registry.Registry) client.Client {
	if dt > 0 {
		// time for waiting command response
		hystrix.DefaultTimeout = dt
	}
	if sw > 0 {
		// how long to open circuit breaker again
		hystrix.DefaultSleepWindow = sw
	}
	if ept > 0 {
		// percent of bad response
		hystrix.DefaultErrorPercentThreshold = ept
	}
	if mc > 0 {
		// how much request in persecond
		hystrix.DefaultMaxConcurrent = mc
	}
	if vt > 0 {
		// how much requests to decide trip circuit breaker
		hystrix.DefaultVolumeThreshold = vt
	}
	cs := micro.NewService(
		micro.Registry(*registry),
		micro.WrapClient(hystrixplugin.NewClientWrapper()),
		micro.WrapClient(rateplugin.NewClientWrapper(rate)),
		micro.WrapClient(roundrobin.NewClientWrapper()),
		micro.WrapClient(ocplugin.NewClientWrapper(opentracing.GlobalTracer())),
	)
	// set client time out
	cs.Client().Init(
		client.RequestTimeout(time.Second * 10),
	)
	return cs.Client()
}
