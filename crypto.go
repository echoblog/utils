package utils

import (
	"crypto/sha256"
	"encoding/hex"

	"golang.org/x/crypto/bcrypt"
)

// MD5Encode encode string to hash crypto string
func MD5Encode(originstr string) string {
	if originstr == "" {
		return ""
	}
	byetStr := []byte(originstr)
	cryptoStr := sha256.Sum256(byetStr)
	result := hex.EncodeToString(cryptoStr[:])
	return result
}

// BcryptEncode encode string to bcrypt crypto string
func BcryptEncode(originstr string) string {
	bytes, err := bcrypt.GenerateFromPassword([]byte(originstr), bcrypt.DefaultCost)
	if err != nil {
		return ""
	}
	return string(bytes)
}

// BcryptCheck compare password and hash
func BcryptCheck(password string, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
