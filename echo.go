package utils

import (
	"context"

	"github.com/go-playground/validator"
	"github.com/labstack/echo"
	"github.com/micro/go-micro/metadata"
	opentracing "github.com/opentracing/opentracing-go"
)

const (
	// SPAN tracer span
	SPAN = "tracerspan"
	// CTX tracer context
	CTX = "tracerctx"
)

// EchoValidator echo validator
type EchoValidator struct {
	validator *validator.Validate
}

// Validate validating data struct
func (cv *EchoValidator) Validate(i interface{}) error {
	return cv.validator.Struct(i)
}

// NewValidator generate Validator
func NewValidator() *EchoValidator {
	return &EchoValidator{
		validator: validator.New(),
	}
}

// TracerMiddleware echo middleware for tracing http request
func TracerMiddleware() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			var span opentracing.Span
			// start span with empty context
			span, ctx := opentracing.StartSpanFromContext(context.TODO(), c.Request().URL.Path)
			md, ok := metadata.FromContext(ctx)
			if !ok {
				md = make(map[string]string)
			}
			// inject opentracing textmap into empty context, for tracking
			opentracing.GlobalTracer().Inject(span.Context(), opentracing.TextMap, opentracing.TextMapCarrier(md))
			ctx = opentracing.ContextWithSpan(ctx, span)
			ctx = metadata.NewContext(ctx, md)

			defer span.Finish()
			// set span to echo context
			c.Set(SPAN, span)
			// set tracer to echo context
			c.Set(CTX, ctx)
			span.SetTag("http.url", c.Request().URL)
			span.SetTag("http.method", c.Request().Method)
			span.SetTag("http.length", c.Request().ContentLength)
			span.SetTag("http.gateway", c.Request().RemoteAddr)
			span.SetTag("http.ip", c.Request().Header.Get("X-Forwarded-For"))
			next(c)
			span.SetTag("http.status", c.Response().Status)
			return nil
		}
	}
}

// GetContext get tracing context from echo context
func GetContext(c *echo.Context) context.Context {
	return (*c).Get(CTX).(context.Context)
}
